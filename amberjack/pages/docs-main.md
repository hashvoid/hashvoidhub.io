&nbsp;
&nbsp;

* [Servlet-based Deployment](#!/deploy-servlet) explains how you can setup Amberjack in any
environment that supports Java Servlet 3.0 and higher.

* [Deployment in Jetty](#!/deploy-jetty) outlines an integration mechanism between Amberjack and
the Jetty web server using APIs exposed by Jetty.

* [The Bare-bones Container](#!/bare-bones-container) covers the capabilities that are exposed by
the Amberjack container that hosts Web resources and providers. This is useful if you wish to
replace the servlet layer with a completely different transport protocol.

* [Injecting Dependencies](#!/dependency-injection) shows how you can integrate Amberjack with a
dependency injection container such as Guice or Crossbinder. This integration allows Web resources
to declare dependencies on singletons and prototypes that are external to Amberjack and be provided
with the same at runtime.

* [Handling JSON payloads](#!/json-payload) describes how you can use the JSON message body reader
and writer, that comes as an extension to Amberjack, for your application needs.

&nbsp;
&nbsp;

* [Source code Documentation](apidocs/amberjack/index.html) for Amberjack using javadocs.
Right-click to open in a new browser window.
