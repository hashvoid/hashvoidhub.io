&nbsp;

Jetizen is designed to be embedded and invoked from within an application. As the developer, it is
your responsibility to provide the minimal bootstrapping code in order to configure and launch
Jetizen from within a `main()` method.

## The Web Resource

Let us begin by first defining a resource that is JAX-RS compliant and can be exposed through a REST
endpoint for remote invocation over HTTP.

```java
package minimal.app;

@Path("/hello")
public class HelloWorldResource {

	@GET
	public String sayHello() {
		return "hello world";
	}
}
```
In order to bootstrap Jetizen, you need to be aware of and work with the following constructs:

## Configuration

Every Jetizen application requires a minimum set of configuration at the time of launch. This
configuration must be provided in the form of a JSON file. Here is the JSON content for a minimal
application:

```json
{
	"server" : {
		"port" : "8080",
		"amberjack" : {
			"path" : "/api"
		}
	},

	"classpath-scan" : {
		"include-packages" : ["minimal.app"]
	},
}
```

Copy this content into a file named `minimalapp.json` (any other name of your choice will also do).
With this configuration, Jetizen will start the Jetty server on port `8080` and start serving
REST-based resources starting with `api` as the base URI.

The `classpath-scan` JSON field merits further explanation. Jetizen uses reflection mechanisms to
search the classpath for JAX-RS resources, and candidates for dependency injection. The
`classpath-scan` field specified the packages and classes that must be included in the search. In
this example, all paths under the `minimal.app` package, including all nested packages, will be
scanned for JAX-RS and dependency injection elements.

Visit the page [Configuring Jetizen](#!/jetizen-config) for a very detailed understanding on the
configurable aspects of Jetizen and the customization that are possible for a given application.

## Jetizen Application

The integration point with Jetizen is the Java class named `com.hashvoid.jetizen.JetizenApp`. You will need to create a sub-class as per the following code snippet:

```java
public class MinimalApp extends JetizenApp<JetizenConfig> {

	protected void initialize() {
		//do nothing
	}

	protected abstract void dispose() {
		//do nothing
	}
}
```

The `initialize` and `dispose` methods allows you to insert custom logic during the startup and shutdown sequences of Jetizen. For our minimal application, we will leave these methods empty.

The `MinimalApp` is invoked from within the application `main` method as per the following code snippet:

```java
package minimal.app;

public class Main {

	public static void main(String[] args) {
		new MinimalApp().run(args);
	}
}
```

From the command line, invoke the Jetizen app using the following:

```bash
$ /usr/bin/java minimal.app.Main -config minimalapp.json
```

Further parameters for classpath specification, etc. has been omitted from the above command for the sake of brevity. This will start Jetizen and setup the web service with the base url: http://localhost:8080/api/
