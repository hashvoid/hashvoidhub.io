&nbsp;
&nbsp;

Jetizen is intended to be simple and easy to use. The following documents and guides will make you
familiar with the technology and help you to make use of the same in your application.

* [A Minimal Jetizen Application](#!/minimal-app) takes you through the steps of creating a Jetizen
application and expose resources as REST endpoints. It demonstrates how easy it is to setup a Web
service using Jetizen with minimal customizations.

* [Configuring Jetizen](#!/jetizen-config) explores all runtime parameters for Jetizen. Also
explains how to define application-specific configuration paremeters and access it from within code.

* [Extending Jetizen App](#!/extend-jetizen-app) shows how you can customize Jetizen bootstrapping
with application-specific behavior. Describes the features and resources that are at your disposal
in making such changes.

* [Application Life-cycle](#!/lifecycle) describes in details the sequence of steps that are
performed at the time of application startup and shutdown. Also explains how you application logic
fits into these life-cycle sequences.

* [Static Sites](#!/static-site) explains how a Jetizen deployment can be used to serve out static
Web content alongside RESTful services.

* [Administrative Interface](#!/jetizen-admin) shows how you can remotely execute administrative
tasks on a running Jetizen instance.

* [Resolving Dependencies](#!/resolving-dependencies) explains how JAX-RS resources are injected
with singletons, prototypes and provided entities.

* [Setting up Logging](#!/log-setup) suggests how to setup a common logging mechanism across Jetizen's
stack elements and your application.

&nbsp;
&nbsp;

* [Source code Documentation](apidocs/jetizen/index.html) for Jetizen using javadocs. Right-click to
open in a new browser window.

* [Classpath Scanning](#!/classpath-scan) a mechanism for auto identification of entities for the
application layer.
