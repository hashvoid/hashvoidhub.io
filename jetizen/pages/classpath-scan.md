&nbsp;

Classpath scanning is a technique that is used by multiple components within Jetizen to search for
Java classes that have special meaning with each of these components. This search is executed on the
system class path using a combination of byte-code interpretation and Java reflections. The classes
are matched against parent classes and interfaces and against class and method annotations.

The table below summaries the usage of class path scanning within the scope of Jetizen.

| Component Name |  Relevant Entities Identified |
| ----- | ----- |
| [Crossbinder](/crossbinder) | elements of dependency injection (singletons, prototypes and providers), method and lifecycle interceptors. |
| [Amberjack](/amberjack) | Web resources, interceptors, message body readers and writers  that are compliant with JAX-RS 2.0 specification |
| [Conn](http://github.com/hashvoid/conn) | Commands for remotely administering a Jetizen instance |

Classpath scanning is implemented as a library named **classpath**. You can
[access the source code](http://github.com/hashvoid/classpath) on Github.
