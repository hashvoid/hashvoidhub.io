# Binary Distribution

Precompiled binaries of Jetizen are not available for download by conventional means. You will have
to use Maven as the build management tool to download the binaries (and optionally sources and
javadocs) from our repositories.

The following repository configuration must be added to your Maven POM file:

```xml
<repositories>
    <repository>
        <id>randondiesel-release</id>
        <name>Maven Repository for Hashvoid</name>
        <url>http://www.randondiesel.com/artifactory/release</url>
		<snapshots>
            <enabled>false</enabled>
        </snapshots>
    </repository>
	<repository>
        <id>randondiesel-snapshot</id>
        <name>Maven Repository for Hashvoid</name>
        <url>http://www.randondiesel.com/artifactory/snapshot</url>
		<snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
    <repository>
        <id>central</id>
        <name>Central Repository</name>
        <url>http://repo.maven.apache.org/maven2</url>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
    </repository>
</repositories>
```

Note that you will also need to add a configuration for Maven central as our repositories will not
proxy additional artifacts, required by your application, from public repositories.

Add the following fragment under the dependency section of your Maven POM file:

```xml
<dependency>
    <groupId>com.hashvoid</groupId>
    <artifactId>com.hashvoid.jetizen</artifactId>
    <version>{jetizen-version}</version>
</dependency>
```

Replace `{jetizen-version}` with the latest release verison of Jetizen (you may also use a snapshot
version if you want to stay on the bleeding edge).

# Building from Source

The primary code repository for Jetizen is hosted on Github and can be browsed online at
http://github.com/hashvoid/jetizen.

## Pre-requisites

In order to build from source, the developer machine must have the following tools installed:

- **Java Platform Standard Edition (Java SE) version 8**. We recommend either the
[SDK from Oracle](http://www.oracle.com/java) or [Zulu: a certified build of OpenJDK](http://zulu.org/).
Previous versions of Java are not officially supported (although you should be able to backport to
Java 7 with minimum changes).

- **Apache Maven** available from http://maven.apache.org. Maven version 3.3 or later is required.

- **Command-Line Git** available from https://git-scm.com/downloads. You can also use a graphical
git client, based on your familiarity with such tools. However, the rest of the build instructions
will assume a command-line git, making it your responsibility to do the necessary translations for
your choice of git client.

## Build Execution

Clone the source repository from Github. On the command line, enter:

```bash
git clone https://github.com/hashvoid/jetizen.git
```

When building for the first time, you'll need to have an Internet connection, because additional
files need to be downloaded.

Open a terminal/console/command prompt, change to the directory where you cloned jetizen, and type:

```bash
mvn clean package install
```
The folder containing maven executables must be present on your environment's path specification.
Otherwise, you will need to specify the full path to `mvn` in the above instruction. After you run
the instruction, with any luck, there will be all sorts of console spew for a few moments, followed
by a BUILD SUCCESS message. Hooray!

To get the latest updates, just run `git pull` from the command line (or GUI client) and repeat the
two instructions above.

## Integration

After you finish building from source, the Jetizen binaries are placed in a maven repository on the
local machine. In order to use the same, the corresponding application's source project must again
use Maven as the build management tool.

A repository configuration (like the one at the top of this page) will not be necessary, only the
dependencies must be added to the application's Maven POM.

```xml
<dependencies>
...
	<dependency>
	    <groupId>com.hashvoid</groupId>
	    <artifactId>com.hashvoid.jetizen</artifactId>
	    <version>{jetizen-version}</version>
	</dependency>
...
</dependencies>
```
