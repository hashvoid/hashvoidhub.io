&nbsp;

As mentioned elsewhere, in order to bootstrap Jetizen, you must define an application class that
derives from `JetizenApp` as a parameterized type with `JetizenConfig` as the type argument.

```java
public class MyApplication extends JetizenApp<JetizenConfig> {

	// this method is called by Jetizen at the time of startup
	protected void initialize() {
		JetizenConfig config = getConfiguration();
		// use the configuration data here ...
	}
}
```

When you launch the application, an instance of `JetizenConfig` will be instantiated and made
available to your application through convenience methods. The `JetizenConfig` will contain
information that is loaded from a configuration file of your choice.

# Configuration File Contents

The configuration file can contain only information that is encoded using JSON format. Given below
is the JSON for a configuration with all the parameters defined explictly.

```json
{
	"classpath-scan" : {
		"classpaths" : [ "class/path/1", "class/path/2"],
		"libpaths" : ["lib/path/1", "lib/path/2"],
		"include-packages" : [ "minimal.app.one", "minimal.app.two" ],
		"exclude-packages" : [ "minimal.app.three", "minimal.app.four" ],
		"include-classes" : [ "minimal.app.Sample1", "minimal.app.Sample2" ],
		"exclude-classes" : [ "minimal.app.Sample3", "minimal.app.Sample4" ],
		"include-patterns" : [ "pattern1", "pattern2" ],
		"exclude-patterns" : [ "pattern3", "pattern4" ]
	},

	"server" : {
		"host" : "0.0.0.0",
		"port" : "8080",
		"amberjack" : {
			"path" : "/",
			"application-class" : "minimal.app.MyJerseyApp",
			"disable-json-messagebody-converters" : "false"
		},
		"static-sites" : [
			{
				"path" : "/site1",
				"real-path" : "path/to/site1",
				"accept-ranges" : "false",
				"dir-listing" : "false"
			}
		]
	},

	"admin-server" : {
		"host" : "127.0.0.1",
		"port" : "9090",
		"path" : "/admin"
	}
}
```

The purpose of the above fragment is to describe all the configuration options that are at your
disposal. In practice, you will skip most of these in your application configuration and proceed
with default values.

## Classpath Scan

This is a common set of configuration parameters to setup [classpath scanning](#!/classpath-scan)
and used from within Amberjack, Crossbinder and Conn.

|||
| ----- | ----- |
| **classpaths** | specifies additional locations (folder, jar and zip files) that should be scanned for Web resources, injectables and interceptors. The elements of the JVM classpath are implicitly included in this list. You may omit this node from the configuration, in which case scannning would occur on the JVM classpath only.|
| **libpaths** | specifies additional locations as folders on the local file system containing one or more zip and jar files. Each of these zip/jar files are added to the list of classpath entries above for scanning. You may omit this node from the classpath. |
| **include-packages** | a list of fully qualified package names. Classes that are member of these packages, and nested sub-packages, can be setup as Web resources, injectables and interceptors. |
| **exclude-packages** | a list of fully qualified package names. Classes that are member of these packages, and any nested sub-packages, are excluded from being setup as Web resources, injectables and interceptors. Excluded packages provide additional filtering on top of packages and classes that have been marked for inclusion. |
| **include-classes** | a list of fully qualified class names. Classes whose fully qualified names are present in this list can be setup as Web resources, injectables and interceptors. |
| **exclude-classes** | a list of fully qualified class names. Classes whose fully qualified names are present in this list are excluded from being setup as Web resources, injectables and interceptors. Excluded classes provide additional filtering on top of packages and classes that have been marked for inclusion. |
| **include-patterns** | a list of regular expression strings. Classes whose fully qualified names match any one of these regular expressions can be setup as Web resources, injectables and interceptors. |
| **exclude-patterns** | a list of regular expression strings. Classes whose fully qualified names match any one of these regular expressions are excluded from being setup as Web resources, injectables and interceptors. Exclusion patterns provide additional filtering on top of packages and classes that have been marked for inclusion. |

Additional `classpath` and `libpath` configuration are relevant when your application includes its own classloaders that are used to load classes from custom locations that are not mentioned on the JVM classpath.

The configuration must specify at least one package containing scanned entities (through `include-packages`) or at least one class which is a scanned entity (through `include-classes` and `include-patterns`). Otherwise, Jetizen will start normally but not host any Web resource or singleton instances.

## Server

Configures the HTTP server endpoint that is used to host Web services and serve static content.

|||
|----|----|
| **host** | The local IP address or host name for the HTTP server. If the value is `localhost` or `127.0.0.1` then the server will not be accessible from a different machine. Set the value as `0.0.0.0` for the server to be remotely accessible. You may omit this node from the configuration, in which case it defaults to a value of `0.0.0.0`. |
| **port** | port number on which to run the HTTP server. You may omit this node from the configuration, in which case the server will be started on port `8080` |


### Amberjack

This is a nested JSON entity under `server` that is used to configure Amberjack as the JAX-RS container.

|||
|----|----|
| **path** | specifies the base URL relative to which all Web resource endpoints would be created. For example, if the path value is `/api` and a Web resource has a `@Path("/resource")` defined on a method, then the corresponding URL to invoke that method would be `http://<machine.ip.address>:<port>/api/resource`. You may omit this node from the configuration, in which the path value defaults to `/`.  |
| **application-class** | fully qualified class name of a JAX-RS application. If specified, it automatically disables classpath scanning for Web resources, which are then obtained from an instance of the application. Please refer to the JAX-RS specification for a detailed description of the application class. |
| **disable-json-messagebody-converters** | By default, Jetizen includes an implementation of message body reader and writer for media type `application/json`. Setting this value to `true` would disable those reader and writer so that you can include your own for handling JSON encoded request/response bodies.|


### Static Sites

A static site configuration allows for Jetizen to serve static content alongside Web services. More than one static site can be served from a single Jetizen instance. Each static site configuration includes the following parameters:

|||
|-----|-----|
| **path** | specifies the base URL relative to which all static content is provided for a given site.  |
| **real-path** | absolute or relative file system path to a folder containing static site content. |
| **accept-ranges** | enables partial content to be served for requests that includes the `Accept-Ranges` header specifying a byte range to be retrieved. |
| **dir-listing** | enables listing of directory content in addition to retrieval of individual file contents. |


## Admin Server

Configures the HTTP server endpoint that is used to remotely administer a Jetizen instance using [Conn](http://www.github.com/hashvoid/conn). The admin server will not be started if a `admin-server` node is not present in the JSON configuration.

The following parameters are included in the admin server configuration:

|||
|----|----|
| **host** | The local IP address or host name for the admin server. If the value is `localhost` or `127.0.0.1` then the server will not be accessible from a different machine. Set the value as `0.0.0.0` for the server to be remotely accessible. Not having this entry in the configuration would start the server on `0.0.0.0`. |
| **port** | port number on which to run the admin server. Not having this entry in the configuration would start the server on port `9090` |
| **path** | specifies the base URL relative to which all admin command endpoints would be created. For example, if the path value is `/cmd` and a Conn command has a `@CommandName("/stat")` defined, then the corresponding URL to invoke that command would be `http://<machine.ip.address>:<adminport>/cmd/stat`. Not having this entry in the configuration would result in `/` being used as the base URL.  |


# Application-Specific Configuration

The format of the Jetizen configuration file can be extended to also include application-specific configuration. This results in a more streamlined approach of defining, parsing and accessing custom configuration from within applications. A single configuration file also signifies better manageability especially during deployment.

## Custom Configuration Object

As mentioned earlier, the contents of a configuration file are mapped into a `JetizenConfig` instance. In order to have application-specific configuration, this class must be extended to include addiitonal fields as shown in the example below:

```java
public class MyJetizenConfig extends JetizenConfig {

	@JSON("database")
	private DbConfig dbConfig;
}

public class DbConfig {

	@JSON("connection-url")
	private String connectionUrl;

	@JSON("user")
	private String userName;

	@JSON("password")
	private String password;
}
```
Note: Jetizen makes use of [JSONMapper library](http://www.github.com/hashvoid/jsonmapper) to parse
JSON content. Please refer to the corresponding documentation for any queries related to
configuration object implementation details.

## Extended Configuration File Contents

Given below is the JSON for the extended configuration with all application-specific parameters explicitly defined.

```json
{
	"classpath-scan" : {
		"include-packages" : [ "minimal.app.one", "minimal.app.two" ]
	},

	"database" : {
		"connection-url" : "jdbc:mysql://localhost/testdb",
		"user" : "admin",
		"password" : "admin123"
	}
}
```

## Launching with Extended Configuration

Within the application `main()` method, Jetizen is bootstrapped using a class that derives from `JetizenApp` as a parameterized type with `MyJetizenConfig` as the type argument.

```java
public class MyApplication extends JetizenApp<MyJetizenConfig> {

	// this method is called by Jetizen at the time of startup
	protected void initialize() {
		MyJetizenConfig config = getConfiguration();
		// use the application-specific configuration here ...
	}
}
```
