# Amberjack

Amberjack is a cleanroom implementation of the JAX-RS specification. It follows a modular design, with a generic, protocol agnostic container as the core to deploy applications, web resources and providers. For the transport layer, the container can be coupled with a variety of HTTP servers (e.g. Jetty and Tomcat) or dropped into a generic servlet container. For the business layer, it provides integration with a variety of dependency injection containers (Crossbinder being the DI of choice).

The philosophy being Amberjack is threefolds:

1. Provide the building blocks to create opinionated technology stacks. Amberjack by itself comes with a set of pre-defined stacks that mixes and matches between Jetty, Tomcat, Crossbinder and Guice.
2. Serve as a viable and third alternative to DropWizard or Spring Boot as the technology stack to power microservices.
3. Simplify binary footprints of web applications through minimal external dependencies.

```java

import java.io.*;

public class Main() {

}
```
